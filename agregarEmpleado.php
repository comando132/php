<?php
include_once('./includes/autoload.php');
    $pdo = new DbPDO();
    $errors = [];
    if (isset($_POST) && $_POST['agregar']) {
        $emp = new Employee(null, $_POST['firstName'], $_POST['lastName'], $_POST['email'], $_POST['extension'], $_POST['jobTitle'], $_POST['reportsTo'], $_POST['officeCode']);
        $emp->save();
        exit;
    }
    $ciudades = $pdo->query('select distinct(country) from offices order by country', null, PDO::FETCH_CLASS);
    $oficinas = $pdo->query('select officeCode, country, city from offices order by country, city', null, PDO::FETCH_CLASS);
    $jefes = $pdo->query('select employeeNumber, firstName, lastName, jobTitle from employees where employeeNumber <= 1143 order by employeeNumber', null, PDO::FETCH_CLASS);
?>
<!doctype html>
<html lang="es">
    <?php include_once('./includes/header.php'); ?>
    <body cz-shortcut-listen="true">
        <!-- Begin page content -->
        <?php include_once('./includes/menu.php'); ?>
        <main role="main" class="container">
            <form id="frm_employee" method="POST" action="./agregarEmpleado.php">
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="firstName">Nombre(s)</label>
                        <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Nombre(s)">
                        <div class="invalid-feedback"><?= (isset($errors['firstName'])) ? $errors['firstName'] : "" ?></div>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="lastName">Apellido(s)</label>
                        <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Apellido(s)">
                        <div class="invalid-feedback"><?= (isset($errors['lastName'])) ? $errors['lastName'] : "" ?></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="email">Correo</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="correo@classicmodelcars.com">
                        <div class="invalid-feedback"><?= (isset($errors['email'])) ? $errors['email'] : "" ?></div>
                    </div>
                    <div class="form-group col-md-5">
                    <label for="extension">Extension</label>
                        <input type="text" class="form-control" name="extension" id="extension" placeholder="x0000" maxlength="10">
                        <div class="invalid-feedback"><?= (isset($errors['extension'])) ? $errors['extension'] : "" ?></div>
                    </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-5">
                        <label for="jobTitle">Cargo</label>
                        <input type="text" class="form-control" name="jobTitle" id="jobTitle" placeholder="Cargo">
                        <div class="invalid-feedback"><?= (isset($errors['jobTitle'])) ? $errors['jobTitle'] : "" ?></div>
                    </div>
                <div class="form-group col-md-5">
                        <label for="reportsTo">Jefe </label>
                        <select name="reportsTo" id="reportsTo" class="form-control">
                            <option value="">-- N/A --</option>
                            <?php foreach($jefes as $jefe): ?>
                                <option value="<?= $jefe->employeeNumber ?>"><?= "{$jefe->firstName} {$jefe->lastName} - {$jefe->jobTitle}" ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback"><?= (isset($errors['reportsTo'])) ? $errors['reportsTo'] : "" ?></div>
                    </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-5">
                        <label for="city">Ciudad</label>
                        <select name="city" id="city" class="form-control">
                            <option value="">-- Todas --</option>
                            <?php foreach($ciudades as $ciudad): ?>
                                <option value="<?= $ciudad->country ?>"><?= "{$ciudad->country}" ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="officeCode"><i id="officeCodeLoading" class="fas fa-spinner fa-spin d-none">&nbsp;</i>Oficina</label>
                        <select name="officeCode" id="officeCode" class="form-control">
                            <option value="">-- Selecciona una opci&oacute;n --</option>
                            <?php foreach($oficinas as $oficina): ?>
                                <option value="<?= $oficina->officeCode ?>"><?= "{$oficina->country} - {$oficina->city}" ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback"><?= (isset($errors['officeCode'])) ? $errors['officeCode'] : "" ?></div>
                    </div>
                </div>
                <button name="agregar" value="agregar" type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </main>
        <?php include_once('./includes/footer.php'); ?>
        <script src="assets/js/agregarEmpleado.js"></script>
    </body>
</html>