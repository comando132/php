$(document).ready(function() {
    $('#city').on('change', function() {
        $('#officeCodeLoading').removeClass('d-none');
        $.ajax({
             url: 'ajax_oficinas.php',
             method: 'POST',
             // The type of data that you're expecting back from the server (xml, json, script, or html
             dataType: 'html',
             data: {
                 city: $(this).val()
             },
             success: (data, textStatus) => {
                 $('#officeCode').html(data);
             },
             complete: () => {
                 setTimeout(()=>{
                     $('#officeCodeLoading').addClass('d-none');
                 }, 1000);
             }
         });
    });
 });