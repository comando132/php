<?php
// saber que es peticion ajax
if (key_exists('HTTP_X_REQUESTED_WITH', $_SERVER) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
    include_once('./includes/autoload.php');
    $pdo = new DbPDO();
    $list = "<option value=\"\">-- Selecciona una opci&oacute;n --</option>";
    $sql = 'SELECT officeCode, country, city from offices';
    $attrs = null;
    // se revisa si viene el city en el post
    if (isset($_POST['city']) && !empty($_POST['city'])) {
        $sql = 'SELECT officeCode, country, city from offices where country like :ciudad';
        $attrs = ['ciudad' => $_POST['city']];
    }
    // consulta a la base de datos
    $oficinas = $pdo->query($sql, $attrs, PDO::FETCH_CLASS);
    // se genera la lista desplegable
    if (!empty($oficinas)) {
        foreach($oficinas as $oficina) {
            $list .=  "<option value=\"{$oficina->officeCode}\">{$oficina->country} - {$oficina->city}</option>";
        }
    }
    // se imprime la lista desplegable
    echo $list;
} else {
    echo "NO es peticion ajax";
}
