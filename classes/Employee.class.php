<?php
class Employee {
    public $employeeNumber;
    public $firstName;
    public $lastName;
    public $email;
    public $extension;
    public $jobTitle;
    public $reportsTo;
    public $officeCode;
    public $isValidModel;
    public $errors;

    public function __construct($employeeNumber, $firstName, $lastName, $email, $extension, $jobTitle, $reportsTo, $officeCode) {
        //$this->dbo = new DbPDO();
        $this->setEmployeeNumber($employeeNumber);
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        $this->email = $email;
        $this->extension = $extension;
        $this->jobTitle = $jobTitle;
        $this->reportsTo = $reportsTo;
        $this->officeCode = $officeCode;
    }

    public function setEmployeeNumber(?String $employeeNumber): void {
        if (empty($employeeNumber)) {
            $this->employeeNumber = substr(microtime(),0 ,4);
        } else {
            $this->employeeNumber = $employeeNumber;
        }
    }

    public function setFirstName(String $firstName): void {
        $this->firstName = $firstName;
    }

    public function setLastName(String $lastName): void {
        $this->lastName = $lastName;
    }

    public function save() {
        $dbo = new DbPDO();
        $sql = "INSERT INTO employees(firstName, lastName, email, extension, jobTitle, reportsTo, officeCode) VALUES (:firstName, :lastName, :email, :extension, :jobTitle, :reportsTo, :officeCode )";
        return ($dbo->query("INSERT INTO employees(employeeNumber, firstName, lastName, email, extension, jobTitle, reportsTo, officeCode) VALUES (:employeeNumber, :firstName, :lastName, :email, :extension, :jobTitle, :reportsTo, :officeCode )", $this->toArray()) > 0) ? true : false;
    }

    static public function getEmpleados() {
        $dbo = new DbPDO();
        $sql = "SELECT * from employees";
        return $dbo->query($sql, PDO::FETCH_BOTH);
    }

    public function toArray(): Array{
        return [
            'employeeNumber' => $this->employeeNumber,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'extension' => $this->extension,
            'jobTitle' => $this->jobTitle,
            'reportsTo' => $this->reportsTo,
            'officeCode' => $this->officeCode
        ];
    }
}