<?php
spl_autoload_register(function ($clase) {
    $archivo = null;
    if (file_exists("classes/$clase.php")) {
        $archivo = "classes/$clase.php";
    } elseif (file_exists("classes/$clase.class.php")) {
        $archivo = "classes/$clase.class.php";
    }
    if(!empty($archivo)) {
        include_once ($archivo);
    }
});