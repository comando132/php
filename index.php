<?php
    include_once('includes/autoload.php');
    $empleados = Employee::getEmpleados();
?>
<!doctype html>
<html lang="es">
    <?php include_once('includes/header.php'); ?>
    <body cz-shortcut-listen="true">
        <!-- Begin page content -->
        <?php include_once('includes/menu.php'); ?>
        <main role="main" class="container">
            <div class="row justify-content-end">
                <div class="col-1">
                    <a class="btn btn-primary" href="./agregarEmpleado.php">Agregar</a></div>
                </div>
            </div>
            <br class="clearfix" />
            <div class="row">
                <?php foreach($empleados as $emp) : ?>
                    <div class="col-sm-3">
                        <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><?= "{$emp['firstName']} {$emp['lastName']}" ?></h5>
                            <p class="card-text">
                                <strong><?= $emp['jobTitle'] ?></strong><br />
                                <?= $emp['email'] ?> <br />
                                <?= $emp['extension'] ?>
                            </p>
                            <a href="#" class="btn btn-primary">Detalle</a>
                        </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </main>
        <?php include_once('includes/footer.php'); ?>
    </body>
</html>